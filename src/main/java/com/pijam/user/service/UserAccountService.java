package com.pijam.user.service;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pijam.user.authn.UserClaims;
import com.pijam.user.clients.application.ApplicationClient;
import com.pijam.user.exception.NotRegisteredException;
import com.pijam.user.model.Onboarding;
import com.pijam.user.model.Plan;
import com.pijam.user.model.UserAccount;
import com.pijam.user.repository.UserAccountRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserAccountService {

    @Autowired
    private UserAccountRepository repository;
    
    @Autowired
    private ConcurrentMapCache cache;
    
    @Autowired
    private ApplicationClient client;
    
    @Value("${pijam.trial.duration}")
    private int trialDuration;
    
    public Optional<UserAccount> get() {
        String email = UserClaims.get("email").asString();
        
        @SuppressWarnings("unchecked")
        Optional<UserAccount> account = cache.get(email, Optional.class);
        if ( account != null ) {
            return account;
        }
        
        account = repository.findById(email);
        
        if ( account.isPresent() ) {
            UserAccount userAccount = account.get().setLastAccess(new Date());
            account = Optional.of(repository.save(userAccount));
            cache.put(email, account);
        }
        
        return account;
    }
    
    public UserAccount register(String locale) {
        String email = UserClaims.get("email").asString();
        Date now = new Date();
        UserAccount account = new UserAccount()
                .setEmail(email)
                .setCreated(now)
                .setLastAccess(now)
                .setExpires(new LocalDateTime(now).plusDays(trialDuration).toDate())
                .setPlan(Plan.FISHER)
                .setLocale(locale);
        account = repository.save(account);
        cache.put(email, Optional.of(account));
        return account;
    }
    
    public UserAccount patch(UserAccount patch) {
        UserAccount account = get().orElseThrow(() -> new NotRegisteredException());
        
        patchLocale(account, patch);
        patchOnboarding(account, patch);

        account = repository.save(account);
        cache.put(UserClaims.get("email").asString(), Optional.of(account));
        
        return account;
    }

    @Async
    @Transactional
    public void delete(String email, String token) {
        ResponseEntity<Void> response = client.deleteApplications("Bearer " + token);
        log.debug("DELETE /applications: {}", response.getStatusCode());
        repository.deleteById(email);
        cache.evict(email);
    }

    public void evict() {
        String email = UserClaims.get("email").asString();
        cache.evict(email);
    }
    
    private void patchOnboarding(UserAccount account, UserAccount accountPatch) {
        Onboarding onboardingPatch = accountPatch.getOnboarding();
        
        if ( onboardingPatch != null ) {
            int applicationOnboardingStep = onboardingPatch.getApplicationOnboardingStep();
            int generalOnboardingStep = onboardingPatch.getGeneralOnboardingStep();
            
            Onboarding onboarding = account.getOnboarding();
            onboarding = onboarding == null ? new Onboarding() : onboarding;
            
            if ( generalOnboardingStep > 0 ) {
                onboarding.setGeneralOnboardingStep(generalOnboardingStep);
            }
            
            if ( applicationOnboardingStep > 0 ) {
                onboarding.setApplicationOnboardingStep(applicationOnboardingStep);
            }
            
            account.setOnboarding(onboarding);
        }
    }

    private void patchLocale(UserAccount account, UserAccount accountPatch) {
        String locale = accountPatch.getLocale();
        if ( StringUtils.isNotBlank(locale) ) {
            account.setLocale(locale);            
        }
    }


}
