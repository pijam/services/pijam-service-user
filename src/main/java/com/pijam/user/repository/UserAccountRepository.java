package com.pijam.user.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.pijam.user.model.UserAccount;

public interface UserAccountRepository extends MongoRepository<UserAccount, String> {

}
