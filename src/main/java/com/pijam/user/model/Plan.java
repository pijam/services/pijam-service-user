package com.pijam.user.model;

import lombok.Getter;

@Getter
public enum Plan {
    FISHER,
    HUNTER,
    SNIPER;
    
}
