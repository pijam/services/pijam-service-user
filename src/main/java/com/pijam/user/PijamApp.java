package com.pijam.user;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

import com.google.common.cache.CacheBuilder;
import com.pijam.user.authn.ClaimsFilter;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableAsync
@EnableMongoRepositories
public class PijamApp {

    private static final int APPLICATION_CACHE_TTL_IN_MIN = 60;
    private static final int APPLICATION_MAX_CACHE_ENTRIES = 255;

    public static void main(String[] args) {
        SpringApplication.run(PijamApp.class, args);
    }

    @Bean
    public FilterRegistrationBean<ClaimsFilter> userAccountFilterRegistration() {
        FilterRegistrationBean<ClaimsFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(claimsFilter());
        registration.setUrlPatterns(Arrays.asList("/users/*"));
        return registration;
    }

    @Bean
    public ClaimsFilter claimsFilter() {
        return new ClaimsFilter();
    }

    @Bean
    public ConcurrentMapCache cache() {
        return new ConcurrentMapCache("users",
                CacheBuilder.newBuilder()
                .expireAfterWrite( APPLICATION_CACHE_TTL_IN_MIN, TimeUnit.MINUTES)
                .maximumSize(APPLICATION_MAX_CACHE_ENTRIES).build().asMap(), false);
    }
    
}
