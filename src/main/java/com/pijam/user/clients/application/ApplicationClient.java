package com.pijam.user.clients.application;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value="application")
public interface ApplicationClient {
    @DeleteMapping(path="/applications")
    ResponseEntity<Void> deleteApplications(@RequestHeader("Authorization") String bearerToken);
}
