package com.pijam.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pijam.user.authn.UserClaims;
import com.pijam.user.exception.AlreadyRegisteredException;
import com.pijam.user.exception.NotRegisteredException;
import com.pijam.user.model.UserAccount;
import com.pijam.user.service.UserAccountService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/users")
public class UserAccountController {
    
    @Autowired
    private UserAccountService service;
    
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public UserAccount register(@RequestBody(required=false) UserAccount partialAccount) {
        if ( service.get().isPresent() ) {
            throw new AlreadyRegisteredException(); 
        }
        String initialLocale = partialAccount == null ? null : partialAccount.getLocale();
        return service.register(initialLocale);
    }
    
    @PatchMapping(consumes=MediaType.APPLICATION_JSON_VALUE)
    public UserAccount update(@RequestBody UserAccount patch) {
        if ( !service.get().isPresent() ) {
            throw new NotRegisteredException(); 
        }
        return service.patch(patch);
    }
    
    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteAccount() {
        if ( !service.get().isPresent() ) {
            throw new NotRegisteredException(); 
        }
        
        //passing email and token as parameters because
        //delete is annotated @Async and UserClaims is bound to the current thread
        String email = UserClaims.get("email").asString();
        String token = UserClaims.getClaims().getAuthorization();

        log.debug("deleting user {}", email);

        service.delete(email, token);
    }
    
    @GetMapping
    public UserAccount get() {
        return service.get().orElseThrow(() -> new NotRegisteredException());
    }
    
    @GetMapping("/logout")
    public void logout() {
        service.evict();
    }
    
}