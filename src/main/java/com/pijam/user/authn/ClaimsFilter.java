package com.pijam.user.authn;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.cors.CorsUtils;

public class ClaimsFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            if ( !CorsUtils.isPreFlightRequest((HttpServletRequest) request) ) {
                UserClaims claims = UserClaims.init((HttpServletRequest) request);
                
                if (claims == null ) {
                    ((HttpServletResponse) response).sendError(HttpStatus.UNAUTHORIZED.value());
                    return;
                }
            }
            chain.doFilter(request, response);
        }
        finally {
            UserClaims.reset();
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    
    @Override
    public void destroy() {

    }
    
}
