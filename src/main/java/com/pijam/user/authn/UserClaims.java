package com.pijam.user.authn;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import lombok.Getter;
import lombok.NonNull;


public class UserClaims {
    private static final ThreadLocal<Optional<UserClaims>> claims = ThreadLocal.withInitial(() -> Optional.empty());
    
    @NonNull
    @Getter
    private String authorization;

    private DecodedJWT decodedJwt;

    private UserClaims(String authorization) {
        this.authorization = authorization;
        decodedJwt = JWT.decode(authorization);
    }
    
    
    public static UserClaims getClaims() {
        return claims.get().get();
    }
    
    public static UserClaims init(HttpServletRequest request) {
        if ( claims.get().isPresent() ) {
            return claims.get().get();
        }
        
        String bearer = request.getHeader(HttpHeaders.AUTHORIZATION);
        
        if ( StringUtils.isBlank(bearer) || !StringUtils.startsWith(bearer, "Bearer ")) {
            return null;
        }
        
        String authorization = bearer.substring("Bearer ".length());
        
        try {
            UserClaims userClaims = new UserClaims(authorization);
            claims.set(Optional.of(userClaims));
            return userClaims;
        }
        catch ( Exception e ) {
            return null;
        }
    }
    
    public static Claim get(String key) {
        if ( claims.get().isPresent() ) {
            return claims.get().get().decodedJwt.getClaim(key);
        }
        return null;
    }
    
    public static void reset() {
        claims.remove();
    }
}
