package com.pijam.user.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Document(collection="user")
@Accessors(chain=true)
public class UserAccount {
    @Id
    private String email;
    
    private Date created;
    
    private Date expires;
    
    private Date lastAccess;
    
    private String locale;
    
    private Onboarding onboarding;
    
    private Plan plan;
}
