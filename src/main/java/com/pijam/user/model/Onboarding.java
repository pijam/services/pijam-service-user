package com.pijam.user.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class Onboarding {
    private int generalOnboardingStep;
    
    private int applicationOnboardingStep;
}
