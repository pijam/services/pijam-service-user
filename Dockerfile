FROM openjdk:8-jdk-alpine
EXPOSE 8081
VOLUME /tmp
ARG UNPACKED=unpacked
COPY ${UNPACKED}/BOOT-INF/lib /app/lib
COPY ${UNPACKED}/META-INF /app/META-INF
COPY ${UNPACKED}/BOOT-INF/classes /app
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-cp","app:app/lib/*","com.pijam.user.PijamApp"]