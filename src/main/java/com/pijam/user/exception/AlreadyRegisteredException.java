package com.pijam.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.CONFLICT, reason="User already registered")
public class AlreadyRegisteredException extends RuntimeException {

}
